package com.futuresubject.customer.restcontroller.mail;

import com.futuresubject.customer.dto.mail.MailObject;
import com.futuresubject.customer.service.mail.EmailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mail")
public class MailController {

//    @Autowired
//    private MailService mailService;
//
//    @PostMapping("/send/{mail}")
//    public String sendMail(@PathVariable("mail") String mail,
//                           @RequestBody MailStructure mailStructure) {
//        mailService.sendMail(mail,mailStructure);
//        return "SUCCESS SEND MAIL";
//    }
    @Autowired
    private EmailServiceImpl emailService;
    @PostMapping("/sends")
    public String sendMails(@RequestBody MailObject mailObject) {
        emailService.sendSimpleMessage(mailObject.getTo()
                ,mailObject.getSubject()
                ,mailObject.getText());
        return "SUCCESS SEND MAIL";
    }
}

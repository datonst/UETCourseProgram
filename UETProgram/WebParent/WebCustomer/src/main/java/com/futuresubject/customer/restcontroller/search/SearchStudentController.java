package com.futuresubject.customer.restcontroller.search;

import com.futuresubject.customer.dto.StudentInfoDto;
import com.futuresubject.customer.dto.search.*;
import com.futuresubject.customer.repository.StudentNotFoundException;
import com.futuresubject.customer.service.MarkSubjectService;
import com.futuresubject.customer.service.search.StudentInfoService;
import com.futuresubject.common.entity.Entity.Program;
import com.futuresubject.common.entity.Enum.ProgramType;
import com.futuresubject.common.entity.Enum.RoleType;
import jakarta.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Period;
import java.util.List;

@RestController
@CrossOrigin
public class SearchStudentController {
    @Autowired
    StudentInfoService studentInfoService;

    @Autowired
    MarkSubjectService markSubjectService;


    @GetMapping("/search")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN"})
    public String listStudents() {
        return "";
    }

    @GetMapping("/searchid/{mssv}")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN"})
    public StudentInfoDto searchStudentId(@PathVariable(name="mssv") String mssv) throws StudentNotFoundException {
        return studentInfoService.getStudent(mssv);
    }

    @GetMapping("/searchSubject/{mssv}/{programFullCode}")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN"})
    public List<SubjectInfoDto> searchFinishedSubject(@PathVariable(name="mssv") String mssv,
                                                      @PathVariable(name="programFullCode") String programFullCode,
                                                      @RequestParam(value = "status",required = false) String status,
                                                      @RequestParam(value = "roleType",required = false) RoleType roleType) {
        List<SubjectInfoDto> dtos = null;
        if ("finished".equals(status)) {
             dtos= studentInfoService.getFinishedSubject(mssv, programFullCode, roleType);
        } else if ("unfinished".equals(status)) {
            dtos= studentInfoService.getUnfinishedSubject(mssv, programFullCode,roleType);
        } else {
            dtos = studentInfoService.getAllSubject(mssv, programFullCode,roleType);
        }
        return dtos;
    }

//    @GetMapping("export/excel/searchSubject/{mssv}/{programFullCode}")
//    @ExceptionHandler
//    @ResponseStatus(HttpStatus.OK)
//    public void exportExcel(@PathVariable(name="mssv") String mssv,
//                            @PathVariable(name="programFullCode") String programFullCode,
//                            @RequestParam(value = "status",required = false) String status,
//                            @RequestParam(value = "roleType",required = false) RoleType roleType,
//                            HttpServletResponse httpServletResponse) throws IOException {
//        List<SubjectInfoDto> dtos = null;
//        if ("finished".equals(status)) {
//            dtos= studentInfoService.getFinishedSubject(mssv, programFullCode, roleType);
//        } else if ("unfinished".equals(status)) {
//            dtos= studentInfoService.getUnfinishedSubject(mssv, programFullCode,roleType);
//        } else {
//            dtos = studentInfoService.getAllSubject(mssv, programFullCode);
//        }
//
//        ExcelExporter exporter = new ExcelExporter();
//        List<String> headers = new ArrayList<>();
//        headers.add("STT");
//        headers.add("SubjectName");
//        headers.add("Credit");
//        headers.add("RoleType");
//        headers.add("Mark");
//        List<List<String>> listObject = new ArrayList<>();
//        int stt=1;
//        for (SubjectInfoDto s : dtos) {
//            List<String> gt = new ArrayList<>();
//            stt+=1;
//            gt.add(String.valueOf(stt));
//            if (s.getSubjectName()!=null) {
//                gt.add(s.getSubjectName());
//            } else {
//                gt.add("");
//            }
//            if (s.getCredit()!=null) {
//                gt.add(String.valueOf(s.getCredit()));
//            } else {
//                gt.add("");
//            }
//            if (s.getRoleType()!=null) {
//                gt.add(String.valueOf(s.getRoleType()));
//            } else {
//                gt.add("");
//            }
//            if (s.getMark()!=null) {
//                gt.add(String.valueOf(s.getMark()));
//            } else {
//                gt.add("");
//            }
//            listObject.add(gt);
//        }
//        exporter.export("search",headers,listObject, httpServletResponse);
//    }

//    @GetMapping("/runvalue")
//    @ExceptionHandler
//    @ResponseStatus(HttpStatus.OK)
//    public void value(HttpServletRequest request, HttpServletResponse httpServletResponse) throws IOException {
//        System.out.println("sadfdsfadfdfadfasf");
//        ExcelExporter exporter = new ExcelExporter();
//        List<String> headers = new ArrayList<>();
//        headers.add("STT");
//        headers.add("SubjectName");
//        headers.add("Credit");
//        headers.add("RoleType");
//        headers.add("Mark");
//        List<List<String>> listObject = new ArrayList<>();
//        int stt=1;
//        List<SubjectInfoDto> dtos = studentInfoService.getFinishedSubject("22028245", "CN8-2019", null);
//        for (SubjectInfoDto s : dtos) {
//            List<String> gt = new ArrayList<>();
//            stt+=1;
//            gt.add(String.valueOf(stt));
//            if (s.getSubjectName()!=null) {
//                gt.add(s.getSubjectName());
//            } else {
//                gt.add("");
//            }
//            if (s.getCredit()!=null) {
//                gt.add(String.valueOf(s.getCredit()));
//            } else {
//                gt.add("");
//            }
//            if (s.getRoleType()!=null) {
//                gt.add(String.valueOf(s.getRoleType()));
//            } else {
//                gt.add("");
//            }
//            if (s.getMark()!=null) {
//                gt.add(String.valueOf(s.getMark()));
//            } else {
//                gt.add("");
//            }
//            listObject.add(gt);
//        }
//        exporter.export("search",headers,listObject, httpServletResponse);
//    }


    @GetMapping("/getAverageMark/{mssv}/{programFullCode}")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN"})
    public AverageMark getAverageMarkByRoleType(@PathVariable(name="mssv") String mssv,
                                                @PathVariable(name="programFullCode") String programFullCode,
                                                @RequestParam(value = "roleType",required = false) RoleType roleType) {
            AverageMark averageMark = new AverageMark();
            List<SubjectInfoDto> dtos = studentInfoService.getFinishedSubjectOrder(mssv, programFullCode, roleType);
            Program program = studentInfoService.getProgram(programFullCode);
            averageMark.setAverageMark(
                    studentInfoService
                            .getMaxAverageMark(dtos,program,roleType)
                            .getAverageMark());
            return averageMark;
    }



    @GetMapping("/downgraded/{mssv}/{programFullCode}")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    public DownGrade getDownGraded(@PathVariable(name="mssv") String mssv,
                                   @PathVariable(name="programFullCode") String programFullCode) {
        DownGrade downGrade = new DownGrade();
        downGrade.setDownGrade(studentInfoService.downGraded(mssv, programFullCode));
        return downGrade;
    }



    @GetMapping("/graduation/{mssv}/{programFullCode}")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN"})
    public GraduatedCondition enoughCert(@PathVariable(name="mssv") String mssv,
                                         @PathVariable(name="programFullCode") String programFullCode) {
        return studentInfoService.viewGraduation(mssv,programFullCode);
    }


}

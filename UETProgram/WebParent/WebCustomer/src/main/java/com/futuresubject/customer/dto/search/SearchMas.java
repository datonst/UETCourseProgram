package com.futuresubject.customer.dto.search;

public interface SearchMas  {
    String getStudentId();
    Integer getProgramId();
    Double getSumMark();
}

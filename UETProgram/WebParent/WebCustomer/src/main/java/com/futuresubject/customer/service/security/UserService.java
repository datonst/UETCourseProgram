package com.futuresubject.customer.service.security;



import com.futuresubject.customer.dto.NotFoundDataExeption;
import com.futuresubject.customer.dto.response.UserResponse;
import com.futuresubject.customer.dto.user.CredentialsDto;
import com.futuresubject.customer.dto.user.SignUpDto;
import com.futuresubject.customer.dto.user.UserDto;
import com.futuresubject.customer.exceptions.AppException;
import com.futuresubject.customer.mapper.UserMapper;
import com.futuresubject.customer.repository.RoleRepository;

import com.futuresubject.customer.repository.UserRepository;
import com.futuresubject.common.entity.Account.User;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.nio.CharBuffer;
import java.util.Optional;

@RequiredArgsConstructor
@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Slf4j
public class UserService {
    @Autowired
    private  UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final UserMapper userMapper;

    @Autowired
    private RoleRepository roleRepository;


    public UserDto login(CredentialsDto credentialsDto) {
        User user = userRepository.findByLogin(credentialsDto.getLogin())
                .orElseThrow(() -> new AppException("Unknown user", HttpStatus.NOT_FOUND));

        if (passwordEncoder.matches(CharBuffer.wrap(credentialsDto.getPassword()), user.getPassword())) {
            UserDto userDto =  userMapper.toUserDto(user);
//            userDto.setRole(String.valueOf(user.getRoles().iterator().next().getName()));
            return userDto;
        }
        throw new AppException("Invalid password", HttpStatus.BAD_REQUEST);
    }

    public UserDto register(SignUpDto userDto) {
        Optional<User> optionalUser = userRepository.findByLogin(userDto.getLogin());

        if (optionalUser.isPresent()) {
            throw new AppException("Login already exists", HttpStatus.BAD_REQUEST);
        }

        User user = userMapper.signUpToUser(userDto);
        user.setPassword(passwordEncoder.encode(CharBuffer.wrap(userDto.getPassword())));
        user.addRole( roleRepository.findByName("ROLE_VIEWER").get());
        User savedUser = userRepository.save(user);

        UserDto userDto1=  userMapper.toUserDto(savedUser);
//        userDto1.setRole(String.valueOf(user.getRoles().iterator().next().getName()));
        return userDto1;
    }

    public User findByLogin(String login) {
        User user = userRepository.findByLogin(login)
                .orElseThrow(() -> new AppException("Unknown user", HttpStatus.NOT_FOUND));
        return user;
    }
    public UserResponse getUserInfo() throws NotFoundDataExeption {
        var context = SecurityContextHolder.getContext();
        try {
            UserDto userDto = (UserDto) context.getAuthentication().getPrincipal();
            User user = userRepository.findByLogin(userDto.getLogin()).orElseThrow(() -> new AppException("Unknown user", HttpStatus.NOT_FOUND));
            return userMapper.toUserResponse(user);
        } catch (Exception ex) {
            throw new NotFoundDataExeption("Could not find any user");
        }

}


}

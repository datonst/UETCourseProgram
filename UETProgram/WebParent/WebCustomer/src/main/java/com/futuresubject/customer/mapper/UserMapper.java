package com.futuresubject.customer.mapper;

import com.futuresubject.customer.dto.request.UserUpdateRequest;
import com.futuresubject.customer.dto.response.UserResponse;
import com.futuresubject.customer.dto.user.SignUpDto;
import com.futuresubject.customer.dto.user.UserDto;
import com.futuresubject.common.entity.Account.User;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

//@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "roles",expression="java(user.getRolesString())")
    UserDto toUserDto(User user);
    UserResponse toUserResponse(User user);
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "roles", ignore = true)
    User signUpToUser(SignUpDto signUpDto);

    @Mapping(target = "roles", ignore = true)
    void updateUser(@MappingTarget User user, UserUpdateRequest request);
//    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
//    User partialUpdate(UserDto userDto, @MappingTarget User user);
}

package com.futuresubject.customer.restcontroller.security;

import com.futuresubject.common.entity.Entity.Program;
import com.futuresubject.common.entity.Entity.Student;
import com.futuresubject.customer.dto.MarkSubjectDto;
import com.futuresubject.customer.dto.NotFoundDataExeption;
import com.futuresubject.customer.dto.StudentDto;
import com.futuresubject.customer.dto.response.UserResponse;
import com.futuresubject.customer.dto.search.GraduatedCondition;
import com.futuresubject.customer.dto.search.MarkDto;
import com.futuresubject.customer.mapper.StudentMapper;
import com.futuresubject.customer.repository.StudentNotFoundException;
import com.futuresubject.customer.service.MarkSubjectService;
import com.futuresubject.customer.service.StudentService;
import com.futuresubject.customer.service.search.StudentInfoService;
import com.futuresubject.customer.service.security.UserService;
import jakarta.annotation.security.RolesAllowed;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/info")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Slf4j
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;


//    @PostMapping
//    ApiResponse<UserResponse> createUser(@RequestBody @Valid UserCreationRequest request) {
//        log.info("Controller: create User");
//        return ApiResponse.<UserResponse>builder()
//                .result(userService.createUser(request))
//                .build();
//    }
//
//    @GetMapping
//    ApiResponse<List<UserResponse>> getUsers() {
//        var authentication = SecurityContextHolder.getContext().getAuthentication();
//
//        log.info("Username: {}", authentication.getName());
//        authentication.getAuthorities().forEach(grantedAuthority -> log.info(grantedAuthority.getAuthority()));
//
//        return ApiResponse.<List<UserResponse>>builder()
//                .result(userService.getUsers())
//                .build();
//    }

//    @GetMapping("/{userId}")
//    ApiResponse<UserResponse> getUser(@PathVariable("userId") String userId) {
//        return ApiResponse.<UserResponse>builder()
//                .result(userService.getUser(userId))
//                .build();
//    }

    @GetMapping("/user")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN","ROLE_VIEWER"})
    UserResponse getUserInfo() throws NotFoundDataExeption {
//        return ApiResponse.<UserResponse>builder()
//                .result(userService.getUserInfo())
//                .build();
        return userService.getUserInfo();
    }

}

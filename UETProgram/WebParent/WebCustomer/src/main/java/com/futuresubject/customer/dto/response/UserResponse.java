package com.futuresubject.customer.dto.response;

import com.futuresubject.common.entity.Account.Role;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserResponse {
//    String id;
    String login;
    String firstName;
    String lastName;
//    LocalDate dob;
//    Set<Role> roles;
}

package com.futuresubject.customer.restcontroller;

import com.futuresubject.common.entity.Entity.Student;
import com.futuresubject.common.entity.Enum.RoleType;
import com.futuresubject.customer.dto.ObtainCertDto;
import com.futuresubject.customer.dto.ProgramDto;
import com.futuresubject.customer.dto.StudentDto;
import com.futuresubject.customer.dto.search.GraduatedCondition;
import com.futuresubject.customer.dto.search.SubjectInfoDto;
import com.futuresubject.customer.mapper.StudentMapper;
import com.futuresubject.customer.repository.StudentNotFoundException;
import com.futuresubject.customer.service.AttendanceService;
import com.futuresubject.customer.service.MarkSubjectService;
import com.futuresubject.customer.service.ObtainCertService;
import com.futuresubject.customer.service.search.StudentInfoService;
import jakarta.annotation.security.RolesAllowed;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
@RestController
@RequestMapping("/info")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Slf4j
@CrossOrigin
public class StudentInfoController {
    @Autowired
    StudentInfoService studentInfoService;
    @Autowired
    MarkSubjectService markSubjectService;
    @Autowired
    ObtainCertService obtainCertService;
    @Autowired
    AttendanceService attendanceService;
    @GetMapping("/student")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN","ROLE_VIEWER"})
    StudentDto getStudentInfo() throws StudentNotFoundException {
//        return ApiResponse.<UserResponse>builder()
//                .result(userService.getUserInfo())
//                .build();
        Student student = studentInfoService.getStudentInfo();
        return StudentMapper.INSTANCE.toDto(student);
    }
//    @DeleteMapping("/{userId}")
//    ApiResponse<String> deleteUser(@PathVariable String userId) {
//        userService.deleteUser(userId);
//        return ApiResponse.<String>builder().result("User has been deleted").build();
//    }
//
//    @PutMapping("/{userId}")
//    ApiResponse<UserResponse> updateUser(@PathVariable String userId, @RequestBody UserUpdateRequest request) {
//        return ApiResponse.<UserResponse>builder()
//                .result(userService.updateUser(userId, request))
//                .build();
//    }

    @GetMapping("/graduation/{programFullCode}")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN","ROLE_VIEWER"})
    GraduatedCondition viewGraduationInfo(@PathVariable(name = "programFullCode") String programFullCode) throws StudentNotFoundException {
//        return ApiResponse.<UserResponse>builder()
//                .result(userService.getUserInfo())
//                .build();
        Student student = studentInfoService.getStudentInfo();
        System.out.println(student);
        return studentInfoService.viewGraduation(student.getStudentId(),programFullCode);
    }

//    @GetMapping("/grade/list/{programFullCode}")
//    @ExceptionHandler
//    @ResponseStatus(HttpStatus.OK)
//    @RolesAllowed({"ROLE_ADMIN","ROLE_VIEWER"})
//    List<SubjectInfoDto> viewGradeInfo(@PathVariable(name = "programFullCode") String programFullCode) throws StudentNotFoundException {
////        return ApiResponse.<UserResponse>builder()
////                .result(userService.getUserInfo())
////                .build();
//        Student student = studentInfoService.getStudentInfo();
//        return studentInfoService.getFinishedSubject(student.getStudentId(),programFullCode,null);
//    }

    @GetMapping("/mark/{programFullCode}")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    public List<SubjectInfoDto> searchFinishedSubject(@PathVariable(name="programFullCode") String programFullCode,
                                                      @RequestParam(value = "status",required = false) String status,
                                                      @RequestParam(value = "roleType",required = false) RoleType roleType) throws StudentNotFoundException {
        List<SubjectInfoDto> dtos = null;
        Student student = studentInfoService.getStudentInfo();
        String mssv = student.getStudentId();
        if ("finished".equals(status)) {
            dtos= studentInfoService.getFinishedSubject(mssv, programFullCode, roleType);
        } else if ("unfinished".equals(status)) {
            dtos= studentInfoService.getUnfinishedSubject(mssv, programFullCode,roleType);
        } else {
            dtos = studentInfoService.getAllSubject(mssv, programFullCode,roleType);
        }
        return dtos;
    }

    @GetMapping("/obtaincert")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN","ROLE_VIEWER"})
    List<ObtainCertDto> viewObtainCertInfo() throws StudentNotFoundException {
//        return ApiResponse.<UserResponse>builder()
//                .result(userService.getUserInfo())
//                .build();
        Student student = studentInfoService.getStudentInfo();
        return obtainCertService.getByStudentId(student.getStudentId());

    }

    @GetMapping("/program")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN","ROLE_VIEWER"})
    List<ProgramDto> viewProgramFullCodeInfo() throws StudentNotFoundException {
//        return ApiResponse.<UserResponse>builder()
//                .result(userService.getUserInfo())
//                .build();
        return studentInfoService.getAllProgram();
    }

    @GetMapping("/listrole")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN","ROLE_VIEWER"})
    public List<RoleType> listRoleType() {
        return Arrays.asList(RoleType.values());
    }
}

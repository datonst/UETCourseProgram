package com.futuresubject.admin.restcontroller.security;


import com.futuresubject.admin.config.security.UserAuthenticationProvider;
import com.futuresubject.admin.dto.user.CredentialsDto;
import com.futuresubject.admin.dto.user.SignUpDto;
import com.futuresubject.admin.dto.user.UserDto;
import com.futuresubject.admin.exceptions.AppException;
import com.futuresubject.admin.mapper.UserMapper;
import com.futuresubject.admin.repository.RoleRepository;
import com.futuresubject.admin.repository.UserRepository;
import com.futuresubject.admin.service.security.UserService;
import com.futuresubject.common.entity.Account.Role;
import com.futuresubject.common.entity.Account.User;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.nio.CharBuffer;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
public class AuthController {

    private final UserService userService;
    private final UserAuthenticationProvider userAuthenticationProvider;

    @PostMapping("/login")
    public ResponseEntity<UserDto> login(@RequestBody @Valid CredentialsDto credentialsDto) {
        UserDto userDto = userService.login(credentialsDto);
        userDto.setToken(userAuthenticationProvider.createToken(userDto.getLogin()));
        return ResponseEntity.ok(userDto);
    }

    @PostMapping("/register")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN"})
    public ResponseEntity<UserDto> register(@RequestBody @Valid SignUpDto user) {
        UserDto createdUser = userService.register(user);
        createdUser.setToken(userAuthenticationProvider.createToken(user.getLogin()));
        return ResponseEntity.created(URI.create("/users/" + createdUser.getId())).body(createdUser);
    }



    @PostMapping("/register_list")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN"})
    public void registesr(@RequestBody @Valid List<SignUpDto> userDto_list) {
        System.out.println("GET_data");
        userService.Student_list(userDto_list);
    }

    @GetMapping("/listauthorizedroles")
    @ExceptionHandler
    @ResponseStatus(HttpStatus.OK)
    @RolesAllowed({"ROLE_ADMIN"})
    public List<String> listAuthorizedRoles() {
        return userService.listAuthorizedRoles();
    }
}
